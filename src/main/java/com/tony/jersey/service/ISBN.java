/**
 * 
 */
package com.tony.jersey.service;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author tony
 *
 */
//@XmlRootElement
public class ISBN {
	String isbn;
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public ISBN(){
		super();
	}
	public ISBN(String isbn){
		this.isbn = isbn;
	}
}
