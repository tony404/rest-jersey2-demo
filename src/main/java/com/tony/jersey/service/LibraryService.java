/**
 * 
 */
package com.tony.jersey.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

/**
 * @author tony
 *
 */

public interface LibraryService {
	public String getBooks();
	public Book getBook(ISBN id);
	public Response addBook(Book book);
	public void removeBook(@QueryParam("id") String id);
}
