package com.tony.jersey.service;

import java.io.IOException;
import java.math.BigDecimal;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;


public class MyClientRequestFilter implements ClientRequestFilter {

	@Override
	public void filter(final ClientRequestContext rc) throws IOException {
		String method = rc.getMethod();
		if ("POST".equals(method) && rc.hasEntity()) {
			Book book = (Book) rc.getEntity();
			book.setId(new ISBN("4567"));
			rc.setEntity(book);
		}
	}

}
