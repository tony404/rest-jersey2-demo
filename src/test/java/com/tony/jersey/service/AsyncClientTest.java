/**
 * 
 */
package com.tony.jersey.service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.InvocationCallback;

import org.junit.Test;


/**
 * @author tony
 *
 */
public class AsyncClientTest {
	
	Client client = ClientBuilder.newClient();
	@Test
	public void test() {
		Future<String> future = client.target("http://localhost:8080/rest-jersey2-demo/service/library/books").request().async()
		.get(new InvocationCallback<String>() {
			@Override
			public void completed(final String books) {
				System.out.println(" books received");
				
			}

			@Override
			public void failed(final Throwable throwable) {
				throwable.printStackTrace();
			}
		});
		try {
			String result = future.get();
			System.out.println("result:" + result);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
	}

}
