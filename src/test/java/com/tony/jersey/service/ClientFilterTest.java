/**
 * 
 */
package com.tony.jersey.service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.junit.Test;


/**
 * @author wangtianzhi
 *
 */
public class ClientFilterTest {
	@Test
	public void test() {
		Client client = ClientBuilder.newClient().register(JacksonJsonProvider.class).register(MyClientRequestFilter.class);
		
		Book book = new Book();
		ISBN isbn = new ISBN();
		isbn.setIsbn("6789");
		book.setId(isbn);
		book.setName("thinking in java");
		Response res = client
		.target("http://localhost:8080/rest-jersey2-demo/service/library/book")
		.request()
		.post(Entity.entity(book, MediaType.APPLICATION_JSON),
				Response.class);
		System.out.println(res.getStatus());
	}
}
